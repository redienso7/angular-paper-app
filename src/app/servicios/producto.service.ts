import { Injectable } from '@angular/core';
import { Producto } from '../entidades/producto';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductoService {

  private tablaProductos = new Map<number | string, Producto>();
  private _tablaProductosSubject = new Subject<Map<number | string, Producto>>();

  constructor() {
    this.initMockProductos();
    this.actualizar();
  }

  public darTablaProductoObservable(): Observable<Map<number | string, Producto>> {
    return this._tablaProductosSubject.asObservable();
  }
  
  public darProducto(codigo: string): Promise<Producto> {
    return Promise.resolve(this.tablaProductos.get(codigo));
  }

  public actualizar(): void {
    this._tablaProductosSubject.next(this.tablaProductos);
  }

  public agregarProducto(elProducto: Producto): Promise<Producto> {
    this.tablaProductos.set(elProducto.codigo, elProducto);
    this.actualizar();
    return Promise.resolve(elProducto);
  }

  public editarProducto(productoAEditar: Producto): Promise<Producto> {
    this.tablaProductos.set(productoAEditar.codigo, productoAEditar);
    this.actualizar();
    return Promise.resolve(productoAEditar);
  }

  public eliminarProducto(elProducto: Producto): Promise<boolean> {
    const isEliminado = this.tablaProductos.delete(elProducto.codigo);
    this.actualizar();
    return Promise.resolve(isEliminado);
  }

  private initMockProductos(): void {
    this.tablaProductos.set(
      "lwfi283r",
      {
        codigo: "lwfi283r",
        nombre: "producto 1",
        marca: "marca 1",
        bodega: "Bodega # 1",
        urlFoto: "",
      }
    );
    this.tablaProductos.set(
      "283jslkd",
      {
        codigo: "283jslkd",
        nombre: "producto 2",
        marca: "marca 2",
        bodega: "Bodega # 1",
        urlFoto: "",
      }
    );
    this.tablaProductos.set(
      "f8kd2dss",
      {
        codigo: "f8kd2dss",
        nombre: "producto 3",
        marca: "marca 3",
        bodega: "Bodega # 1",
        urlFoto: "",
      }
    );
  }

}
