import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SnackbarService {

  mensajeSubject = new Subject<String>();

  constructor() { }

  darObservableMensaje(): Observable<String>{
    return this.mensajeSubject.asObservable();
  }

  mostrarMensaje(mensaje: string){
    this.mensajeSubject.next(mensaje);
    this.mostrarSnackbar();
  }

  private mostrarSnackbar(): void {
    const snackbar = document.getElementById("snackbar");
    snackbar.className = "show";
    setTimeout(() => {
      snackbar.className = snackbar.className.replace("show", "");
    }, 3000);
  }


}
