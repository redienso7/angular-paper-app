import { Injectable } from '@angular/core';

@Injectable()
export class ModalService {

  private btnOpenModal;
  private btnCerrarModal;

  constructor() { }

  mostrarModal(): void{
    this.btnOpenModal = document.getElementById("btnOpenModal");
    this.btnOpenModal.click();
  }

  cerrarModal(): void {
    this.btnCerrarModal = document.getElementById("btnCerrarModal");
    this.btnCerrarModal.click();
  }

}
