import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';

import { ProductoService } from './servicios/producto.service';

import { AppComponent } from './app.component';
import { CabeceraComponent } from './componentes/layout/cabecera/cabecera.component';
import { BarraNavegacionComponent } from './componentes/layout/barra-navegacion/barra-navegacion.component';
import { CuerpoComponent } from './componentes/layout/cuerpo/cuerpo.component';
import { PiePaginaComponent } from './componentes/layout/pie-pagina/pie-pagina.component';
import { ListadoProductosComponent } from './componentes/productos/listado-productos/listado-productos.component';
import { FormularioProductoComponent } from './componentes/productos/formulario-producto/formulario-producto.component';
import { DetalleProductoComponent } from './componentes/productos/detalle-producto/detalle-producto.component';
import { IntroduccionComponent } from './componentes/layout/introduccion/introduccion.component';
import { SnackbarComponent } from './componentes/layout/snackbar/snackbar.component';
import { SnackbarService } from './servicios/snackbar.service';
import { ModalComponent } from './componentes/compartidos/modal/modal.component';
import { ModalService } from './servicios/modal.service';
import { ModalConfirmacionEliminarComponent } from './componentes/productos/modal-confirmacion-eliminar/modal-confirmacion-eliminar.component';
import { TarjetaComponent } from './componentes/compartidos/tarjeta/tarjeta.component';

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    BarraNavegacionComponent,
    CuerpoComponent,
    PiePaginaComponent,
    ListadoProductosComponent,
    FormularioProductoComponent,
    DetalleProductoComponent,
    IntroduccionComponent,
    SnackbarComponent,
    ModalComponent,
    ModalConfirmacionEliminarComponent,
    TarjetaComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: IntroduccionComponent
      },
      {
        path: 'listadoProductos',
        component: ListadoProductosComponent
      },
      {
        path: 'formularioProducto',
        component: FormularioProductoComponent
      },
      {
        path: 'formularioProducto/:codigo',
        component: FormularioProductoComponent
      },
      {
        path: 'detalleProducto/:codigo',
        component: DetalleProductoComponent
      }
    ])
  ],
  providers: [
    SnackbarService,
    ProductoService,
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
