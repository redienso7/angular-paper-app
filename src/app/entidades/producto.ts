export interface Producto{
    codigo: string;
    nombre: string;
    marca: string;
    bodega: string;
    urlFoto: string;
    proveedor?: string;
    stock?: string;
}    