import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'paper-cabecera',
  template: `
    <div class="row justify-content-md-center align-items-center" 
      style="background:transparent url('assets/paper-image.jpg') no-repeat center center /cover; min-height: 200px">
      <div class="col-lg-6">
        <div class="text-center text-muted">
          <h1 style="color:rgb(255,255,255);">PaperWeb</h1>
          <hr>
          <h5 style="color:rgba(255,255,255,0.8);">Maneja tu inventario en la web</h5>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class CabeceraComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
