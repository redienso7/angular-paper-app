import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'paper-cuerpo',
  template: `
    
    <router-outlet></router-outlet>

  `,
  styles: []
})
export class CuerpoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
