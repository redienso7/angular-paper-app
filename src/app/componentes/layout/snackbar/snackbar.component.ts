import { Component, OnInit } from '@angular/core';
import { SnackbarService } from '../../../servicios/snackbar.service';

@Component({
  selector: 'paper-snackbar',
  template: `
    <div id="snackbar">
      {{mensaje}}
    </div>
  `,
  styleUrls: ['./snackbar.component.css']
})
export class SnackbarComponent implements OnInit {

  mensaje = "";

  constructor(private snacbarService: SnackbarService) { 
    this.snacbarService.darObservableMensaje().subscribe((mensaje: string) => {
      this.mensaje = mensaje;
    });
  }

  ngOnInit() {
  }

}
