import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from '../../../servicios/modal.service';

@Component({
  selector: 'paper-modal',
  templateUrl: './modal.component.html',
  styles: []
})
export class ModalComponent implements OnInit {

  @Input('idModal') idModal = "defaultId";
  @Input('titulo') titulo = "defaultTitulo";
  @Input('nombreBtnCerrar') nombreBtnCerrar = "Cerrar";

  constructor() { 
  }
  
  ngOnInit() {
  }

}
