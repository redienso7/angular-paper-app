import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'paper-tarjeta',
  template: `
    <div class="card">
      <div class="card-header">
        {{cabecera}}
      </div>
      <ng-content></ng-content>
      <div class="card-footer">
        {{pie}}
      </div>
    </div>
  `,
  styles: []
})
export class TarjetaComponent implements OnInit {

  @Input('cabecera') cabecera: string = "cabecera por defecto";
  @Input('pie') pie: string = "pie por defecto";

  constructor() { }

  ngOnInit() {
  }

}
