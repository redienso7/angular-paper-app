import { Component, OnInit } from '@angular/core';
import { Producto } from '../../../entidades/producto';
import { ProductoService } from '../../../servicios/producto.service';
import { ModalService } from '../../../servicios/modal.service';
import { Router } from '@angular/router';
import { ModalConfirmacionEliminarComponent } from '../modal-confirmacion-eliminar/modal-confirmacion-eliminar.component';

@Component({
  selector: 'paper-listado-productos',
  templateUrl: 'listado-productos.component.html',
  styles: []
})
export class ListadoProductosComponent implements OnInit {

  isCargando: boolean = true;
  private listadoProductos: Producto[] = [];

  constructor(
    private productoService: ProductoService,
    private modalService: ModalService,
    private router: Router
  ) {
    this.productoService.darTablaProductoObservable().subscribe(tablaProductos => {
      const size = tablaProductos.size;
      const values = tablaProductos.values();
      const productos = [];
      for (let i = 1; i <= size; i++) {
        productos.push(values.next().value);
      }
      this.listadoProductos = productos;
      this.isCargando = false;
    });
    this.productoService.actualizar();
  }

  ngOnInit() {
  }

  irAProductoForm(producto: Producto): void {
    this.router.navigate(['/formularioProducto', producto.codigo]);
  }

  verDetalle(producto: Producto): void {
    this.router.navigate(['/detalleProducto', producto.codigo]);
  }

  confirmarEliminacion(producto: Producto, modalConfirmacionEliminacion: ModalConfirmacionEliminarComponent): void {
    this.modalService.mostrarModal();
    modalConfirmacionEliminacion.setProducto(producto);
  }

  nuevoProducto(): void {
    this.router.navigate(['/formularioProducto']);
  }

}
