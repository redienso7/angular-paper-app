import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmacionEliminarComponent } from './modal-confirmacion-eliminar.component';

describe('ModalConfirmacionEliminarComponent', () => {
  let component: ModalConfirmacionEliminarComponent;
  let fixture: ComponentFixture<ModalConfirmacionEliminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfirmacionEliminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmacionEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
