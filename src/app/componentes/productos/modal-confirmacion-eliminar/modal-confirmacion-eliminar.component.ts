import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../../servicios/producto.service';
import { Producto } from '../../../entidades/producto';
import { SnackbarService } from '../../../servicios/snackbar.service';
import { ModalService } from '../../../servicios/modal.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'paper-modal-confirmacion-eliminar',
  template: `
    <paper-modal idModal="confirmacionEliminarId" titulo="¿Desea eliminar el producto?" nombreBtnCerrar="Cancelar">
      <div cuerpo-modal *ngIf="producto">
          {{producto.nombre}}
      </div>
      <div pie-modal>
          <button type="button" (click)="eliminarProducto()" class="btn btn-secundary">Aceptar</button>
      </div>
    </paper-modal>
  `,
  styles: []
})
export class ModalConfirmacionEliminarComponent implements OnInit {

  private producto: Producto; 

  constructor(private productoService: ProductoService, private snackbarService: SnackbarService, private modalService: ModalService, private rutaActiva: ActivatedRoute) { 
  }

  ngOnInit() {
  }

  public setProducto(producto: Producto): void{
    this.producto = producto;
  }

  private eliminarProducto(): void {
    this.productoService.eliminarProducto(this.producto)
      .then(isProductoEliminado => {
        this.modalService.cerrarModal();
        isProductoEliminado && this.snackbarService.mostrarMensaje("Producto eliminado exitosamente");
      });
  }

}
