import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductoService } from '../../../servicios/producto.service';
import { SnackbarService } from '../../../servicios/snackbar.service';
import { Producto } from '../../../entidades/producto';

@Component({
  selector: 'paper-formulario-producto',
  templateUrl: './formulario-producto.component.html',
  styles: []
})
export class FormularioProductoComponent implements OnInit {

  productoForm: FormGroup;
  producto: Producto;

  constructor(
    private localtion: Location,
    private router: Router,
    private fb: FormBuilder,
    private productoService: ProductoService,
    private snackbarService: SnackbarService,
    private rutaActiva: ActivatedRoute
  ) { }

  ngOnInit() {
    this.rutaActiva.params.subscribe(parametros => {
      if(parametros.codigo){
        this.productoService.darProducto(parametros.codigo).then(producto => {
          this.producto = producto;
          this.construirProductoForm();
        });
      }
    });
  }

  construirProductoForm(): void {
    const producto = this.producto;
    this.productoForm = this.fb.group({
      codigo: [producto ? producto.codigo : '', Validators.required],
      nombre: [producto ? producto.nombre : '', Validators.required],
      marca: producto ? producto.marca : '',
      proveedor: producto ? producto.proveedor : '',
      stock: [producto ? producto.stock : '', Validators.required],
      bodega: [producto ? producto.bodega : '', Validators.required],
      urlFoto: producto ? producto.urlFoto : ''
    });
  }

  irAtras(): void {
    this.localtion.back();
  }

  guardar(): void {
    console.log("guardando..");
    if (this.producto) {
      this.productoService.editarProducto(this.productoForm.value)
        .then(productoEditado => {
          console.log("Producto editado exitosamente. ", productoEditado)
          this.snackbarService.mostrarMensaje("Producto editado exitosamente");
          this.router.navigate(['/listadoProductos']);
        });
      } else {
        this.productoService.agregarProducto(this.productoForm.value)
        .then(productoGuardado => {
          console.log("Producto guardado exitosamente. ", productoGuardado);
          this.snackbarService.mostrarMensaje("Producto guardado exitosamente");
          this.router.navigate(['/listadoProductos']);
        });
    }
  }

}
