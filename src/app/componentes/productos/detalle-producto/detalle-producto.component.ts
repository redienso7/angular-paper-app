import { Component, OnInit } from '@angular/core';
import { Producto } from '../../../entidades/producto';
import { ProductoService } from '../../../servicios/producto.service';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'paper-detalle-producto',
  templateUrl: 'detalle-producto.component.html',
  styles: []
})
export class DetalleProductoComponent implements OnInit {

  private producto: Producto;

  constructor(private productoService: ProductoService, private rutaActiva: ActivatedRoute, private location: Location) { 
  }

  ngOnInit() {
    this.rutaActiva.params.subscribe(parametros => {
      if(parametros.codigo){
        this.productoService.darProducto(parametros.codigo).then(producto => this.producto = producto);
      }
    });
  }

  volver(): void {
    this.location.back();
  }

}
